package com.example

import java.io.{BufferedWriter, File, FileWriter}

import agent.ontology.OntologyManagerActor
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import com.argumentation.{DocumentActor}
import org.apache.commons.io.FileUtils
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}


class UpdateOntologySpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  def this() = this(ActorSystem("ArgumentationActorSystem"))
  val filesToDel: Seq[String] = Seq()

  override def afterAll {
    filesToDel.foreach((filename: String) => FileUtils.deleteQuietly(new File(filename)))
    TestKit.shutdownActorSystem(system)
  }

  "An ontology actor" must {
    "load ontology from IRI" in {
      val ontActor = system.actorOf(OntologyManagerActor.props)
    }
  }

  "A ontology actor" must {
    "receive and proceed msg with SymbolicSentence" in {
      // val ontActorRef = TestActorRef[UpdateOntologyActor]
      val fileContent = "Penguins can fly."
      val filename = "test_update_ontology_actor_rcv_msg.txt"
      val file = new File(filename)
      val bw = new BufferedWriter(new FileWriter(file))
      val ontActor = system.actorOf(OntologyManagerActor.props)
      val documentActor = system.actorOf(DocumentActor.props)


      filesToDel :+ filename
      bw.write(fileContent)
      bw.close()
      documentActor ! DocumentActor.ProcessDocumentRequest(file)

      //sentenceActor ! sentenceActor.toSymbolicSentence()
      //expectMsg(PongActor.PongMessage("pong1"))
    }
  }

}
