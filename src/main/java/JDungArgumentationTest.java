import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;

/**
 * Pamietac, zeby zmienic public na private przy metodach z klasy JDungArgumentation!!!
 */
public class JDungArgumentationTest {
    JDungArgumentation jDungArgumentation1;
    JDungArgumentation jDungArgumentation2;
    JDungArgumentation jDungArgumentation3;
    JDungArgumentation jDungArgumentation4;

    @Before
    public void initTest () {
        JArgument arg0 = new JArgument("0", 0);
        JArgument arg1 = new JArgument("1", 1);
        JArgument arg2 = new JArgument("2", 2);
        JArgument arg3 = new JArgument("3", 3);
        JArgument arg4 = new JArgument("4", 4);
        JArgument arg5 = new JArgument("5", 5);
        JArgument arg6 = new JArgument("6", 6);
        JArgument arg7 = new JArgument("7", 7);

        JArgument carg0 = new JArgument("c0", 0);
        JArgument carg1 = new JArgument("c1", 1);
        JArgument carg2 = new JArgument("c2", 2);
        JArgument carg3 = new JArgument("c3", 3);
        JArgument carg4 = new JArgument("c4", 4);

        JArgument car0 = new JArgument("ca0", 0);
        JArgument car1 = new JArgument("ca1", 1);
        JArgument car2 = new JArgument("ca2", 2);
        JArgument car3 = new JArgument("ca3", 3);
        JArgument car4 = new JArgument("ca4", 4);

        JArgument ar0 = new JArgument("ar0", 0);
        JArgument ar1 = new JArgument("ar1", 1);
        JArgument ar2 = new JArgument("ar2", 2);
        JArgument ar3 = new JArgument("ar3", 3);
        JArgument ar4 = new JArgument("ar4", 4);

        ArrayList<JArgument>TC1 = new ArrayList<>();

        arg0.addAttackBy(arg1);
        arg0.addAttackBy(arg2);

        arg1.addAttackBy(arg3);
        arg1.addAttackBy(arg4);

        arg2.addAttackBy(arg5);

        arg3.addAttackBy(arg6);

        arg4.addAttackBy(arg7);

        TC1.add(arg0);
        TC1.add(arg1);
        TC1.add(arg2);
        TC1.add(arg3);
        TC1.add(arg4);
        TC1.add(arg5);
        TC1.add(arg6);
        TC1.add(arg7);

        ArrayList<JArgument> TC2 = new ArrayList<>();

        carg0.addAttackBy(carg1);
        carg0.addAttackBy(carg2);

        carg2.addAttackBy(carg3);
        carg3.addAttackBy(carg4);
        carg4.addAttackBy(carg2);

        TC2.add(carg0);
        TC2.add(carg1);
        TC2.add(carg2);
        TC2.add(carg3);
        TC2.add(carg4);


        car0.addAttackBy(car1);
        car0.addAttackBy(car3);

        car1.addAttackBy(car2);
        car2.addAttackBy(car1);

        car3.addAttackBy(car4);
        car4.addAttackBy(car3);

        ArrayList<JArgument> TC3 = new ArrayList<>();

        TC3.add(car0);
        TC3.add(car1);
        TC3.add(car2);
        TC3.add(car3);
        TC3.add(car4);

        ar0.addAttackBy(ar1);
        ar0.addAttackBy(ar2);

        ar2.addAttackBy(ar3);

        ar3.addAttackBy(ar4);

        ar4.addAttackBy(ar0);

        ArrayList<JArgument> TC4 = new ArrayList<>();

        TC4.add(ar0);
        TC4.add(ar1);
        TC4.add(ar2);
        TC4.add(ar3);
        TC4.add(ar4);

        jDungArgumentation1 = new JDungArgumentation(TC1);
        jDungArgumentation2 = new JDungArgumentation(TC2);
        jDungArgumentation3 = new JDungArgumentation(TC3);
        jDungArgumentation4 = new JDungArgumentation(TC4);

    }

    @Test
    public void testFindCycles() {
        assertEquals(false, jDungArgumentation1.findCycles());
        assertEquals(true, jDungArgumentation2.findCycles());
        assertEquals(true, jDungArgumentation3.findCycles());
    }

    @Test
    public void testGetResult() {
        assertEquals("Disproved", jDungArgumentation1.getResult());
        assertEquals("Disproved", jDungArgumentation2.getResult());
        assertEquals("OK", jDungArgumentation3.getResult());
    }

    @Test
    public void testFindLeaves() {
        jDungArgumentation1.findCycles();
        jDungArgumentation2.findCycles();
        jDungArgumentation3.findCycles();

        assertEquals(3, jDungArgumentation1.findLeaves().size());
        assertEquals(1, jDungArgumentation2.findLeaves().size());
        assertEquals(1, jDungArgumentation3.findLeaves().size());

        jDungArgumentation1.printStatusForAllArguments();
        jDungArgumentation2.printStatusForAllArguments();
        jDungArgumentation3.printStatusForAllArguments();
    }

    @Test
    public void testVisualizeArgumentationGraph() {
        jDungArgumentation1.getResult();
        jDungArgumentation1.visualizeArgumentationGraph("TC1.png");

        jDungArgumentation2.getResult();
        jDungArgumentation2.visualizeArgumentationGraph("TC2.png");

        jDungArgumentation3.getResult();
        jDungArgumentation3.visualizeArgumentationGraph("TC3.png");

        jDungArgumentation4.getResult();
        jDungArgumentation4.visualizeArgumentationGraph("TC4.png");
    }

}