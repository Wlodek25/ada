import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.VisualizationImageServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import org.apache.commons.collections15.Transformer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

/**
 * All copyrights reserved by ©Patryk Wąsowski
 */
public class JDungArgumentation {
    private JArgument hypothesis;
    private ArrayList<JArgument> arguments;
    private int lastIndex;
    private Graph<JArgument, String> graph;

    public JDungArgumentation(JArgument hypothesis) {
        this.hypothesis = hypothesis;
        arguments = new ArrayList<>();
        lastIndex = 0;

        findAllCounterArguments(hypothesis);
        // generateArguments(0); -- wydaje sie niepotrzebne, byc moze wystarczy dodawac do listy podczas tworzenia kontrarg.
    }

    public JDungArgumentation(ArrayList<JArgument> arguments) {
        this.hypothesis = arguments.get(0);
        this.arguments = arguments;
        this.lastIndex = arguments.size()-1;
    }

    public String getHypothesis() {
        return hypothesis.getContent();
    }

    public String getResult() {
        if(findCycles()) {
            checkHypothesisUndec();
        }
        getArgumentationResult();
        printStatusForAllArguments();

        if(hypothesis.getLabel() == JArgument.Label.IN) {
            return "OK";
        }
        else if(hypothesis.getLabel() == JArgument.Label.OUT) {
            return "Disproved";
        }
        else {
            return "Undecided";
        }
    }

    /**
     * Funkcja rekurencyjna, ktora dla zadanego argumentu szuka kontrargumentow i dla nich rowniez jest wywolywana
     *
     * @param argument - argument, dla ktorego poszukiwane sa kontrargumenty
     */
    private void findAllCounterArguments(JArgument argument) {
        ArrayList<JArgument> counterArguments = getCounterArguments(argument);

        for(JArgument counterArgument : counterArguments) {
            argument.addAttackBy(counterArgument);
            findAllCounterArguments(counterArgument);
        }
    }

    private void getArgumentationResult() {
        while(hypothesis.getLabel() == JArgument.Label.EMPTY) {
            ArrayList<JArgument> leaves = findLeaves();
//            dla kazdego liscia nalezy sprawdzic, czy istnieje jakis argument, ktory go atakuje i ma status IN - wtedy lisc ma status OUT
//            jesli wszystkie argumenty atakujace lisc maja status OUT, wtedy lisc otrzymuje status IN
            for(JArgument leaf : leaves) {
                leaf.setIn();
                for (JArgument attacker : leaf.getAttackedBy()) {
                    if (attacker.getLabel() == JArgument.Label.IN) {
                        leaf.setOut();
                        break;
                    }
                }
            }
        }
    }

    public ArrayList<JArgument> findLeaves() {
        ArrayList<JArgument> list = new ArrayList<>();
        //            liscie, czyli najdalsze od hipotezy wierzcholki, ktore wciaz maja nieokreslony status

        // jesli wierzcholek ma status EMPTY, a wszystkie atakujace go argumenty maja status inny niz EMPTY, to jest to lisc
        for(JArgument argument : arguments) {
            if(argument.getLabel() == JArgument.Label.EMPTY) {
                list.add(argument);
                for(JArgument attacker : argument.getAttackedBy()) {
                    if(attacker.getLabel() == JArgument.Label.EMPTY) {
                        list.remove(argument);
                        break;
                    }
                }
            }
        }
        return list;
    }

    private ArrayList<JArgument> getCounterArguments(JArgument argument) {
        ArrayList<JArgument> list = new ArrayList<>();

        // TODO: rozgloszenie tresci argumentu

        // TODO: stworzenie nowego argumentu z kolejnym numerem indexu
        list.add(new JArgument("Content", ++lastIndex));

        // TODO: dolaczenie nowego argumentu do listy attackekBy parametru

        return list;
    }

    protected boolean findCycles() {
        Stack stack = new Stack();
        Stack stack2 = new Stack();
        Boolean[] visited = new Boolean[lastIndex+1];

        boolean result = false;

        for(int i = 0; i < lastIndex+1; ++i) {
            for(int j = 0; j < visited.length; ++j) {
                visited[j] = false;
            }

            if(findCycle(arguments, arguments.get(i), arguments.get(i), stack, visited)) {
                while(!stack.empty()) {
                    JArgument arg = (JArgument) stack.pop();
                    arg.setUndec();
                    System.out.print(arg.getContent());
                }
                result = true;
            }
        }
        return result;
    }

    private boolean findCycle(ArrayList<JArgument> arguments, JArgument starting, JArgument current, Stack stack, Boolean[] visited) {
        visited[current.getIndex()] = true;
        stack.push(current);

        ArrayList<JArgument> attackers = current.getAttackedBy();
        int i = 0;
        while(i < attackers.size()) {
            JArgument attacker = attackers.get(i);
            if (attacker.equals(starting)) {
                return true;
            }

            if(!visited[attacker.getIndex()] && findCycle(arguments, starting, attacker, stack, visited)) {
                return true;
            }

            ++i;
        }
        stack.pop();
        return false;
    }

    /**
     * Jesli przynajmniej jeden z argumentow atakujacych teze nie nalezy do zadnego cyklu, co jest rownoznaczne ze statusem EMPTY,
     * to nalezy zmienic status tezy na EMPTY, gdyz mozliwe, ze pomimo tego, ze teza nalezy do cyklu, to i tak bedzie ona zbijana przez ktorys kontrargument
     */
    private void checkHypothesisUndec() {
        if(hypothesis.getLabel() == JArgument.Label.UNDEC) {
            for(JArgument argument : hypothesis.getAttackedBy()) {
                if(argument.getLabel() == JArgument.Label.EMPTY) {
                    hypothesis.setEmpty();
                    break;
                }
            }
        }
    }

    public void printStatusForAllArguments() {
        for(JArgument argument : arguments) {
            System.out.println(argument.getContent() + ": " + argument.getLabel().toString());
        }
    }

    private void createGraph() {
        graph =  new SparseMultigraph<JArgument, String>();
        for(JArgument argument : arguments) {
            graph.addVertex(argument);
        }


        int i = 0;
        for(JArgument argument :arguments) {
            for(JArgument attacker : argument.getAttackedBy()) {
                graph.addEdge(Integer.toString(i++), attacker, argument, EdgeType.DIRECTED);
            }
        }
    }

    public void visualizeArgumentationGraph(String fileName) {
        createGraph();

        Layout<JArgument, String> layout = new ISOMLayout(graph);
        layout.setSize(new Dimension(440, 440));
        BasicVisualizationServer<JArgument, String> bvs = new BasicVisualizationServer<JArgument, String>(layout);
        bvs.setPreferredSize(new Dimension(450, 450));

        Transformer<JArgument, Paint> vertexPaint = new Transformer<JArgument, Paint>() {
            @Override public Paint transform(JArgument jArgument) {
                if(jArgument.getLabel() == JArgument.Label.IN) {
                    return Color.GREEN;
                }
                else if(jArgument.getLabel() == JArgument.Label.OUT) {
                    return Color.RED;
                }
                else if(jArgument.getLabel() == JArgument.Label.UNDEC) {
                    return Color.YELLOW;
                }
                else return Color.WHITE;
            }
        };

        bvs.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        bvs.getRenderContext().setVertexFillPaintTransformer(vertexPaint);

        // Create the VisualizationImageServer
        // vv is the VisualizationViewer containing my graph
        VisualizationImageServer<JArgument, String> vis =
                new VisualizationImageServer<JArgument, String>(bvs.getGraphLayout(),
                        bvs.getGraphLayout().getSize());

        // Configure the VisualizationImageServer the same way
        // you did your VisualizationViewer. In my case e.g.

        vis.setBackground(Color.WHITE);
        vis.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
        vis.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());

        // Create the buffered image
        BufferedImage image = (BufferedImage) vis.getImage(
                new Point2D.Double(bvs.getGraphLayout().getSize().getWidth() / 2,
                        bvs.getGraphLayout().getSize().getHeight() / 2),
                new Dimension(bvs.getGraphLayout().getSize()));

        // Write image to a png file
        File outputfile = new File(fileName);

        try {
            ImageIO.write(image, "png", outputfile);
        } catch (IOException e) {
            System.out.println("Nie moge zapisac grafu do pliku");
        }
    }
}