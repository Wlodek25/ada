package com.argumentation;
import java.util.ArrayList;

/**
 * Created by Patryk on 2016-12-30.
 */
public class JArgument {
    private String content;
    private ArrayList<JArgument> attackedBy;
    private Label label;
    private int index;
    private int determinedAttackers;
    private String subject;

    public enum Label {
        IN,
        OUT,
        UNDEC,
        EMPTY
    }

    public JArgument(String content, int index) {
        this.content = content;
        attackedBy = new ArrayList<>();
        label = Label.EMPTY;
        this.index = index;
        determinedAttackers = 0;
    }

    public void addAttackBy(JArgument argument) {
        attackedBy.add(argument);
    }

    public String getContent() {
        return content;
    }

    public ArrayList<JArgument> getAttackedBy() {
        return attackedBy;
    }

    public void setIn() {
        label = Label.IN;
    }

    public void setOut() {
        label = Label.OUT;
    }

    public void setUndec() {
        label = Label.UNDEC;
    }

    public void setEmpty() {
        label = Label.EMPTY;
    }

    public Label getLabel() {
        return label;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setDeterminedAttackers() {
        determinedAttackers++;
    }

    public int getDeterminedAttackers() {
        return determinedAttackers;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return Integer.toString(index);
    }
}
