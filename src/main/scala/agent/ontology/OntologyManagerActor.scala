package agent.ontology

import java.io.File

import scala.collection.JavaConverters._
import akka.actor.{Actor, ActorLogging, Props}
import com.argumentation.DocumentActor.ProcessedMessage
import com.argumentation.{DocumentActor, Predicate, SymbolicSentence}
import edu.stanford.nlp.ie.machinereading.domains.ace.reader.MatchException
import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat
import org.semanticweb.owlapi.model._
import org.semanticweb.owlapi.reasoner.InferenceType
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory
import uk.ac.manchester.cs.owl.owlapi.{OWLDataPropertyImpl, OWLObjectPropertyImpl, OWLObjectUnionOfImpl}
import org.semanticweb.owlapi.search.EntitySearcher
import scala.collection.mutable

class OntologyManagerActor extends Actor with ActorLogging {
  import OntologyManagerActor._

  var counter = 0
  val ontActor = context.actorOf(OntologyManagerActor.props, "updateOntologyActor")

  def receive = {
  	case Initialize =>
	    log.info("In UpdateOntologyActor - starting")
    case UpdateOntologyRequest(symbolicSentence) =>
      val sentence = symbolicSentence
      log.info(s"Received sentence: $sentence")
      updateKnowledgeBase(sentence)
      context.parent ! DocumentActor.
        ProcessedMessage(context.self.path.name, symbolicSentence.toString)
    case GetObjectsWithPropertyValue(propertyName, propertyValue, sentence, argumentIndex) =>
      log.info(s"Searching objects with property $propertyName equals $propertyValue")
      getObjectsWithPropertyValue(propertyName, propertyValue, s"", -1)

  }

  def updateKnowledgeBase(sentence: SymbolicSentence) = {
    log.info(s"Updating Knowledge base with sentence: $sentence")
    val subject = sentence.subject
    val predicate = sentence.predicate
    val determiner = sentence.determiner
    val negated = sentence.negated
    val `object` = sentence.`object`.lift(0).get
    val propertyName = predicate + `object`.capitalize

    log.info(s"Predicate value:${predicate.toString} , class: ${predicate.getClass.getName}")
    val ontologySubject = try {
      findSubjectInOntology(ontology, subject)
    }
    catch {
      case me: MatchException => {
        log.info(s"Creating individual named $subject")
        createIndividual(ontology, subject)
      }
    }
    val ontologyObject = try {
     findSubjectInOntology(ontology, `object`)
    }
    catch {
      case me: MatchException => {
        log.info(s"Creating individual named $subject")
      }
    }

    predicate.toUpperCase match {
      case Predicate.Be.entryName => {
        log.info("UpdateOntology: Predicate BE")
        log.info(s"$subject is instance of class ${`object`}")
      }
      case Predicate.Can.entryName | Predicate.Have.entryName => {
        log.info(s"UpdateOntology: Predicate $predicate")
        addObjectProperty(ontology, subject.capitalize, propertyName, "true")
        addObjectProperty(ontology, subject.capitalize, propertyName, "false")
        isObjectSubclassOf(s"Penguin", s"Bird")
        isObjectSubclassOf(s"Buffalo", s"Bird")
        isObjectSubclassOf(s"Buffalo", s"Animals")
        getObjectsWithPropertyValue("canFly", "true", "sentence", -1)
        getObjectsWithPropertyValue("canFlyNot", "true", "sentence", -1)

      }
    }
  }

  def findSubjectInOntology(ontology: OWLOntology, subject: String): Either[OWLClass, OWLNamedIndividual] = {
    val objectName = subject.capitalize
    try {
      val ontClass = findClassInOntology(ontology, objectName)
      Left(ontClass)
    }
    catch {
      case me: MatchException => {
        log.info(s"Searching individual named $subject")
        return Right(findIndividualInOntology(ontology, objectName))
      }
    }
  }

  def findClassInOntology(ontology: OWLOntology, subject: String): OWLClass = {
    val classes = ontology.getClassesInSignature()

    classes.toArray.foreach({ontObject =>
      val ontClass = ontObject.asInstanceOf[OWLClass]
      val ontClassName = ontClass.getIRI.getShortForm.toLowerCase
      val subjectLowerCase = subject.toLowerCase
      ontClassName match {
        case`subjectLowerCase`  =>
          log.info(s"Found class $subject in knowledge base")
          return ontClass
        case notFoundSubject =>
          log.debug(s"Comparing class $notFoundSubject with $subject ")
      }
    })
    throw new MatchException(s"Class named $subject not found in knowledge base")
  }

  def findIndividualInOntology(ontology: OWLOntology, individualName: String): OWLNamedIndividual = {
    val individuals = ontology.getIndividualsInSignature()
    val individualNameLower = individualName.toLowerCase
    individuals.toArray.foreach({individual =>
      individual.asInstanceOf[OWLNamedIndividual].getIRI.getShortForm.toLowerCase match {
        case `individualNameLower` =>
          log.info(s"Found individual $individual in knowledge base")
          return individual.asInstanceOf[OWLNamedIndividual]
        case notFoundIndividual =>
          log.debug(s"Comparing individual $notFoundIndividual with $individual")
      }
    })
    throw new MatchException(s"Individual named $individualName not found in knowledge base")
  }

  def createIndividual(ontology: OWLOntology, individualName: String): Either[Unit, OWLNamedIndividual] = {
    val ontClass = findClassInOntology(ontology, "Animals")
    val newIndividual = ontologyFactory.getOWLNamedIndividual(IRI.create(s"$ns#${individualName.capitalize}"))
    val newAxiom = ontologyFactory.getOWLClassAssertionAxiom(ontClass, newIndividual)
    val ontChange = ontologyManager.addAxiom(ontology, newAxiom)
    Right(newIndividual)
  }

  def setIndividualClass(ontology: OWLOntology, individualName: String, individualClass: String) {
    val ontClass = findClassInOntology(ontology, individualClass)
    val individual = findIndividualInOntology(ontology, individualName)
    val newAxiom =  ontologyFactory.getOWLClassAssertionAxiom(ontClass, individual)
    ontologyManager.addAxiom(ontology, newAxiom)
  }

  def addObjectProperty(ontology: OWLOntology, objectName: String, propName: String, propertyValue: String): Unit = {
    val propertyLiteral = ontologyFactory.getOWLLiteral(true)
    val propertyName = {
      if(propertyValue.toBoolean) {
        propName
      }
      else {
        propName + "Not"
      }
    }
    val propertyIRI = IRI.create(s"$ontologyIRI#$propertyName")
    val ontObject = findSubjectInOntology(ontology, objectName)

    ontObject match {
      case Left(ontClass) =>
        log.info(s"Adding property $propertyName: $propertyValue to class ${ontClass.getIRI.getShortForm.toLowerCase}")
        val property = ontologyFactory.getOWLDataProperty(propertyIRI)
        val propertyHasValue = ontologyFactory.getOWLDataHasValue(property, propertyLiteral)
        val axiom = ontologyFactory.getOWLSubClassOfAxiom(ontClass, propertyHasValue)
        ontologyManager.addAxiom(ontology, axiom)
        ontologyManager.addAxiom(ontology, ontologyFactory.getOWLDataPropertyDomainAxiom(property, ontClass))
        val assertProperty = ontologyFactory.getOWLClass(objectName.capitalize)
      case Right(ontIndividual) =>
        log.info(s"Adding property $propertyName: $propertyValue to individual ${ontIndividual.getIRI.getShortForm.toLowerCase}")
        val property = ontologyFactory.getOWLDataProperty(propertyIRI)
        val axiom = ontologyFactory.getOWLDataPropertyAssertionAxiom(property, ontIndividual, propertyLiteral)
        ontologyManager.addAxiom(ontology, axiom)
        // test
        val file = new File("example_ontology.owl")
        val destination = IRI.create(file)
        ontologyManager.saveOntology(ontology, new OWLXMLDocumentFormat(), destination)
        getObjectsWithPropertyValue(propertyName, "true", "a", -1)
    }
  }

  def IfObjectExists(ontology: OWLOntology, objectName: String) {
    try {
      findSubjectInOntology(ontology, objectName)
      log.debug(s"Object $objectName exists")
      return true
    }
    catch {
      case me: MatchException => {
        return false
      }
    }
  }

  def isObjectSubclassOf(objectName: String, className: String): Boolean = {

    val ontSuperClass = findSubjectInOntology(ontology, className)
    val objectNameLowerCase = {
      findSubjectInOntology(ontology, objectName) match {
        case Left(ontClass) =>
          objectName.toLowerCase
        case Right(ontIndividual) =>
          var ontIndividualClassName = s""
          val ontIndividualClasses = EntitySearcher.getTypes(ontIndividual, ontology).toArray.foreach(
            ontIndividualClass => {
              ontIndividualClassName = ontIndividualClass.asInstanceOf[OWLClass].getIRI.getShortForm
              log.info(s"Individual ${objectName} class is $ontIndividualClassName")
            })
          ontIndividualClassName.toLowerCase
      }
    }

    if(objectNameLowerCase.equals(className.toLowerCase)) {
      log.info(s"${objectName} is subclass of ${className}")
      return true
    }

    ontSuperClass match {
      case Left(ontClass) =>
        val ontSubClasses = ontologyReasoner.getSubClasses(ontClass, true).getFlattened()
        ontSubClasses.toArray.foreach(f = ontSubObj => {
          val ontSubClass = ontSubObj.asInstanceOf[OWLClass]
          try {
            ontSubClass.getIRI.getShortForm.toLowerCase match {
              case `objectNameLowerCase` =>
                log.info(s"${objectName} is subclass of ${className}")
                return true
            }
          }
          catch {
            case me: MatchError =>
              log.debug(s"${objectName} is not sublcasses of ${ontSubClass.getIRI.getShortForm}")
          }
        })
        return false
      case Right(ontIndividual) =>
        val ontSubClasses = EntitySearcher.getTypes(ontIndividual, ontology)
        true
    }
  }

  def getObjectsWithPropertyValue(propName: String, propertyValue: String, sentence: String, argIndex: Int): Seq[String] = {
    var objectsWithProperties = mutable.MutableList[String]()
    val classes = ontology.getClassesInSignature()
    val propertyName = {
      if(propertyValue.toBoolean) {
        propName
      }
      else {
        propName + "Not"
      }
    }
    val propertyIRI = IRI.create(s"$ns#$propertyName")

    classes.toArray.foreach({ontObject =>
      val ontClass = ontObject.asInstanceOf[OWLClass]
      val ontClassName = ontClass.getIRI.getShortForm.toLowerCase

      ontology.getAxioms(AxiomType.OBJECT_PROPERTY_DOMAIN).toArray.foreach (
        axiom => {
          val domainAxioms = axiom.asInstanceOf[OWLObjectPropertyDomainAxiom]
          val searchedProperty = domainAxioms.getProperty.asInstanceOf[OWLObjectPropertyImpl].getIRI.getShortForm
          //log.info(s"Searching property ${searchedProperty}")
          if(searchedProperty.equals(propertyName)) {
            val classesWithProperty: List[OWLClass] = {
              if (domainAxioms.getDomain.isOWLClass) {
                val list = List[OWLClass]() :+ domainAxioms.getDomain
                list.asInstanceOf[List[OWLClass]]
              }
              else {
                var list = mutable.MutableList[OWLClass]()
                domainAxioms.getDomain.
                  asInstanceOf[OWLObjectUnionOfImpl].
                  getOperandsAsList.
                  asScala.foreach(operand => {
                  list += ontologyFactory.getOWLClass(operand.toString)
                })
                list.toList
              }
            }

            classesWithProperty.foreach(classWithProperty => {
              if (classWithProperty.equals(ontClass)) {
                domainAxioms.getDomain.getObjectPropertiesInSignature.toArray.foreach(
                  property => {
                    val objectProperty = property.asInstanceOf[OWLObjectProperty]
                    val foundPropertyName = objectProperty.getIRI.getShortForm.toLowerCase
                    if (propertyName.equals(foundPropertyName)) {
                      log.info(s"$ontClassName has property ${foundPropertyName}")
                      objectsWithProperties += ontClassName.toLowerCase
                    }
                  })
              }
            })
          }
        }
      )
      ontology.getAxioms(AxiomType.DATA_PROPERTY_DOMAIN).toArray.foreach (
        axiom => {
          val domainAxioms = axiom.asInstanceOf[OWLDataPropertyDomainAxiom]
          val searchedProperty = domainAxioms.getProperty.asInstanceOf[OWLDataPropertyImpl].getIRI.getShortForm
          if(searchedProperty.equals(propertyName)) {
            val classesWithProperty: List[OWLClass] = {
              if (domainAxioms.getDomain.isOWLClass) {
                val list = List[OWLClass]() :+ domainAxioms.getDomain
                list.asInstanceOf[List[OWLClass]]
              }
              else {
                var list = mutable.MutableList[OWLClass]()
                domainAxioms.getDomain.
                  asInstanceOf[OWLObjectUnionOfImpl].
                  getOperandsAsList.
                  asScala.foreach(operand => {
                  list += ontologyFactory.getOWLClass(operand.toString)
                })
                list.toList
              }
            }

            classesWithProperty.foreach(classWithProperty => {
              val classWithPropertyName = classWithProperty.getIRI.getShortForm.toLowerCase
              log.info(s"getObjectsWithPropertyValue: comparing class  ${ontClassName}  with $classWithPropertyName ")
              if (classWithPropertyName.equals(ontClassName.toLowerCase)) {
                log.info(s"Found class with property $propertyName: $ontClassName")
                objectsWithProperties += ontClassName.toLowerCase
              }
            })
          }
        }
      )
    })
    val instanceDataProperty = ontologyFactory.getOWLDataProperty(propertyIRI)
    val hasProperty = ontologyFactory.getOWLDataHasValue(instanceDataProperty, ontologyFactory.getOWLLiteral(true))
    //val individualsWithProperty = ontologyReasoner.getInstances(hasProperty, false).asScala

    val individualsWithProperty = classes.toArray.foreach(ontObject => {
      val ontClass = ontObject.asInstanceOf[OWLClass]
      ontologyReasoner.getInstances(ontClass, false).getFlattened.asScala.foreach(
        individual => {
          val individualName = individual.getIRI.getShortForm
          val dataProperties = ontology.getDataPropertyAssertionAxioms(individual).toArray.foreach(
            dataPropAxiom => {
              val dataProp = dataPropAxiom.asInstanceOf[OWLDataPropertyAssertionAxiom].getProperty
              val dataProperty = dataProp.asInstanceOf[OWLDataProperty]
              val individualPropertiesValues = ontologyReasoner.getDataPropertyValues(individual, dataProperty)
              individualPropertiesValues.toArray.foreach(
                individualPropertyValue => {
                  log.info(s"For class ${ontClass.getIRI.getShortForm} individual $individualName found ${dataProperty.getIRI.getShortForm} value ${individualPropertyValue.asInstanceOf[OWLLiteral].getLiteral}")
                  objectsWithProperties += individualName.toLowerCase
                }
              )
            }
          )

        }
      )
    })
    objectsWithProperties.distinct
  }
}



object OntologyManagerActor {
  val ns = "http://protege.stanford.edu/junitOntologies/testset/animals-vh.owl"
  val ontologyIRI = IRI.create(ns)
  val ontologyManager = OWLManager.createOWLOntologyManager()
  val ontologyFactory = ontologyManager.getOWLDataFactory
  val ontology = ontologyManager.loadOntology(ontologyIRI)
  val ontologyReasoner = new StructuralReasonerFactory().createReasoner(ontology)
  ontologyReasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY)

  val predicates = Predicate
  val props = Props[OntologyManagerActor]
  case object Initialize
  case class UpdateOntologyRequest(symbolicSentences: SymbolicSentence)
  case class IfObjectExists(objectName: String)
  case class GetObjectProperty(objectName: String, propertyName: String)
  case class GetObjectsWithPropertyValue(propertyName: String, propertyValue: String, sentence: String, argIndex: Int)
  case class IsObjectClassOf(objectName: String, className: String)
  case class IsObjectSubclassOf(objectName: String, className: String)
}