package com.argumentation

import akka.actor.{Actor, Props}
import com.argumentation.DungActor.GetCounterArgumentRequest

object DungActor {

  val props = Props[DungActor]
  case class HypothesisMessage(msg: SymbolicSentence)
  case class CounterArgumentMessage(msg: SymbolicSentence, index: Integer)
  case class GetCounterArgumentRequest(msg: SymbolicSentence, subject: String, index: Integer)
}

class DungActor extends Actor {

  var jDungArgumentation: JDungArgumentation = _
  var hypoSubject : String = null
  val counterArgumentActor = context.actorOf(CounterArgumentActor.props, "CounterArgumentActor")

  override def receive = {
    case hypothesis: DungActor.HypothesisMessage => startProcessing(hypothesis)
    case counterArgument: DungActor.CounterArgumentMessage => reactForCounterArgument(counterArgument)
  }

  def startProcessing(hypo: DungActor.HypothesisMessage) = {
    val hypothesis = new JArgument(getSentenceFromSymbolicSentence(hypo.msg), 0)
    hypothesis.setDeterminedAttackers()
    jDungArgumentation = new JDungArgumentation(hypothesis)
    jDungArgumentation.getArgumentByIndex(0).setSubject(hypo.msg.subject)
    counterArgumentActor ! CounterArgumentActor.CounterArgumentRequest(hypo.msg, null, 0)
    hypoSubject = hypo.msg.subject
  }

  def reactForCounterArgument(arg: DungActor.CounterArgumentMessage) = {
    var newIndex : Integer = -1
    if(arg.msg != null) {
      newIndex = jDungArgumentation.addCounterArgument(arg.index, getSentenceFromSymbolicSentence(arg.msg))
    }
    jDungArgumentation.getArgumentByIndex(arg.index).setDeterminedAttackers()

    if(newIndex > -1) {
      getCounterArgument(arg.msg, newIndex, hypoSubject)
      jDungArgumentation.getArgumentByIndex(newIndex).setSubject(arg.msg.subject)
    }

    if(jDungArgumentation.allArgumentsHaveDeterminedAttackers()) {
      jDungArgumentation.getResult()
    }
  }

  def getCounterArgument(msg: SymbolicSentence, index: Integer, subject: String) = {
    counterArgumentActor ! CounterArgumentActor.CounterArgumentRequest(msg, subject, index)
  }

  def getSentenceFromSymbolicSentence(symbolicSentence: SymbolicSentence) : String = {
    val stringBuilder = new StringBuilder()

    stringBuilder.append(symbolicSentence.subject)
    if(symbolicSentence.determiner.equals(Determiner.General)) {
      stringBuilder.append("s")
    }

    stringBuilder.append(" ")

    if(symbolicSentence.determiner.equals(Determiner.General)) {
      if(symbolicSentence.predicate.equals(Predicate.Be.entryName.toUpperCase)) {
        stringBuilder.append("are")
      }
      if(symbolicSentence.predicate.equals(Predicate.Can.entryName.toLowerCase)) {
        stringBuilder.append("can")
      }
      if(symbolicSentence.predicate.equals(Predicate.Have.entryName.toLowerCase)) {
        stringBuilder.append("have")
      }
    }

    if(symbolicSentence.determiner.equals(Determiner.Particular)) {
      if(symbolicSentence.predicate.equals(Predicate.Be.entryName.toUpperCase)) {
        stringBuilder.append("is")
      }
      if(symbolicSentence.predicate.equals(Predicate.Can.entryName.toLowerCase)) {
        stringBuilder.append("can")
      }
      if(symbolicSentence.predicate.equals(Predicate.Have.entryName.toLowerCase)) {
        stringBuilder.append("has")
      }
    }

    stringBuilder.append(" ")

    if(symbolicSentence.negated.equals(true)) {
      stringBuilder.append("not ")
    }

    symbolicSentence.`object`.foreach{obj : String =>
      stringBuilder.append(obj)
      stringBuilder.append(" ")
    }

    return stringBuilder.toString()
  }

}