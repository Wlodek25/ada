package com.argumentation

import java.io.File

import akka.actor.{Actor, ActorLogging, Props, Terminated}
import com.argumentation.DocumentActor.ProcessDocumentRequest
import com.argumentation.DungActor.HypothesisMessage
import com.argumentation.MainActor.ProcessedHypothesis

class MainActor extends Actor with ActorLogging {

  var results : Seq[String]= Seq()
  def name = self.path.name
  val textBase: Seq[File] = Seq(new File("allBirdsCanFly.txt"))

  override def receive = {
    case MainActor.Initialize => startProcessing()
    case msg: MainActor.DocumentKnowledgeExtracted => {

      results = results :+ msg.knowledge
      val child = context.child(msg.actorName)
      child.foreach({ c =>
        context.watch(c)
        context.stop(c)
        log.info("Main actor received knowledge extracted by {} from document {}",
        msg.actorName, msg.documentName)})
      MainActor.knowledgeExtracted = true
      if(MainActor.isHypothesisProcessed) {
        startArgumentationActor(MainActor.processedHypothesis)
      }
    }
    case processed: ProcessedHypothesis => {
      results = results :+ processed.message
      val child = context.child(processed.actorName)
      child.foreach { c =>
        context.watch(c)
        context.stop(c)
        log.info("finished processing hypothesis, stopping actor " + c.path.name)
      }
      if(MainActor.knowledgeExtracted) {
        startArgumentationActor(processed)
      } else {
        MainActor.isHypothesisProcessed = true
        MainActor.processedHypothesis = processed
      }
    }
      case Terminated(ref) => {
    if (context.children.isEmpty) log.info("END OF KNOWLEDGE EXTRACTION: "+ results.toString())
else log.info(context.children + "left")
    }
    case x => log.info("didn't recognize the message " + x)
  }

  def initializeDocumentActor(file: File) = {
    val documentActor = context.actorOf(DocumentActor.props, "documentActor" + file.getName)
    documentActor ! ProcessDocumentRequest(file)
  }

  def startProcessing() = {
    val hypothesisActor = context.actorOf(SentenceActor.props, "hypothesisActor")
    hypothesisActor ! HypothesisRequest(readHypothesis)
    textBase.foreach(initializeDocumentActor)
  }
  def readHypothesis: String = {
    scala.io.StdIn.readLine("Gimme the hypothesis!")
  }

  def startArgumentationActor(processedHypothesis: ProcessedHypothesis): Unit = {
    context.actorOf(DungActor.props, "dung-actor") ! HypothesisMessage(processedHypothesis.symbolicHypo)
  }
}

object MainActor {
  val props = Props[MainActor]
  var isHypothesisProcessed = false
  var processedHypothesis = ProcessedHypothesis("NO_ACTOR", "NO_HYPO", SymbolicSentence(s"", Determiner.General, true, s"", s"", Seq[String]()))
  var knowledgeExtracted = false
  case object Initialize
  case object Stop
  case class DocumentKnowledgeExtracted(actorName: String, documentName: String, knowledge: String)
  case class ProcessedHypothesis(actorName: String, message: String, symbolicHypo: SymbolicSentence)
}