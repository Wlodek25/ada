package com.argumentation

import com.argumentation.PartOfSpeech.findValues
import enumeratum.{Enum, EnumEntry}

sealed abstract class PartOfSpeech(override val entryName: String) extends EnumEntry

object PartOfSpeech extends Enum[PartOfSpeech] {
  case object CoordinatingConjunction extends PartOfSpeech("CC")
  case object CardinalNumber extends PartOfSpeech("CD")
  case object Determiner extends PartOfSpeech("DT")
  case object ExistentialThere extends PartOfSpeech("EX")
  case object ForeignWord extends PartOfSpeech("FW")
  case object PrepositionOrSubordinatingConjunction extends PartOfSpeech("IN")
  case object Adjective extends PartOfSpeech("JJ")
  case object AdjectiveComparative extends PartOfSpeech("JJS")
  case object AdjectiveSuperlative extends PartOfSpeech("JJR")
  case object ListItemMarker extends PartOfSpeech("LS")
  case object Modal extends PartOfSpeech("MD")
  case object Noun extends PartOfSpeech("NN")
  case object NounPlural extends PartOfSpeech("NNS")
  case object ProperNounSingular extends PartOfSpeech("NNP")
  case object ProperNounPlural extends PartOfSpeech("NNNPS")
  case object Predeterminer extends PartOfSpeech("PDT")
  case object Preposition extends PartOfSpeech("PRPS")
  case object PossesiveEnding extends PartOfSpeech("POS")
  case object PersonalPronoun extends PartOfSpeech("PRP")
  case object PossesivePronoun extends PartOfSpeech("PRP$")
  case object Adverb extends PartOfSpeech("RB")
  case object AdverbComparative extends PartOfSpeech("RBR")
  case object AdverbSuperlative extends PartOfSpeech("RBS")
  case object Particle extends PartOfSpeech("RP")
  case object Symbol extends PartOfSpeech("SYM")
  case object To extends PartOfSpeech("TO")
  case object Interjection extends PartOfSpeech("UH")
  case object VerbBase extends PartOfSpeech("VB")
  case object VerbPastForm extends PartOfSpeech("VBD")
  case object VerbGerundOrPresentParticiple extends PartOfSpeech("VBG")
  case object VerbPastParticiple extends PartOfSpeech("VBN")
  case object VerbNon3Person extends PartOfSpeech("VBP")
  case object Verb3PersonSingular extends PartOfSpeech("VBZ")
  case object IngVerb extends PartOfSpeech("VBG")
  case object WhDeterminer extends PartOfSpeech("WDT")
  case object WhPronoun extends PartOfSpeech("WP")
  case object WhPossesivePronoun extends PartOfSpeech("WP$")
  case object WhAdvert extends PartOfSpeech("WRB")
  case object Period extends PartOfSpeech(".")
  case object Comma extends PartOfSpeech(",")
  case object QuestionMark extends PartOfSpeech("?")

  val values = findValues
}
