package com.argumentation

import java.util.Properties

import com.ontology.OntologyManagerActor.UpdateOntologyRequest
import akka.actor.{Actor, ActorLogging, Kill, PoisonPill, Props, Terminated}
import com.argumentation.DocumentActor.ProcessedMessage
import com.argumentation.MainActor.DocumentKnowledgeExtracted
import com.ontology.OntologyManagerActor
import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.ling.CoreAnnotations.{LemmaAnnotation, SentencesAnnotation}
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
import edu.stanford.nlp.ling.CoreAnnotations.{LemmaAnnotation, PartOfSpeechAnnotation, SentencesAnnotation}
import enumeratum._
import edu.stanford.nlp.pipeline.StanfordCoreNLP
import edu.stanford.nlp.pipeline.Annotation
import edu.stanford.nlp.trees.{Tree, TreeCoreAnnotations}
import edu.stanford.nlp.util.CoreMap
import enumeratum._

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

class SentenceActor extends Actor with ActorLogging {

  override def postStop() {
    println(s"postStop ${context.self.path.name}")
  }

  def receive = {
    case hypothesisReq: HypothesisRequest =>
      log.info("In SentenceActor - received message process hypo: {}", hypothesisReq.hypothesis)
      val symbolicHypo = processHypo(hypothesisReq.hypothesis).get
      processHypo(hypothesisReq.hypothesis).map(partsOfSpeech => context.parent ! MainActor.ProcessedHypothesis(context.self.path.name, partsOfSpeech.toString, symbolicHypo))
    case request: ProcessSentenceRequest =>
      log.info("In SentenceActor - received message process sentence: {}", request.sentence.sentence)
      val processSent = processSentence(request.sentence)
      processSent.
        map(symbolicSentence => {
          context.actorOf(OntologyManagerActor.props,
            s"updateOntologyActor${request.sentence.sentence.replaceAll(" ", "")}") !
          UpdateOntologyRequest(symbolicSentence)
        })
    case processedMessage: ProcessedMessage =>
      val child = context.child(processedMessage.by)
      child.foreach { c =>
        context.watch(c)
        context.stop(c)
        log.info("received processing confirmation, stopped actor " + c.path.name)
      }
      context.parent ! DocumentActor.ProcessedMessage(context.self.path.name,
        processedMessage.message)
    case Terminated(ref) =>
      log.info(s"Terminated message from ${ref.path.name}")
    case _ => Unit
  }

  def processHypo(sentence: String)(implicit ec: ExecutionContext): Option[SymbolicSentence] = {
    val annotation = new Annotation(sentence.toLowerCase())
    SentenceActor.pipeline.annotate(annotation)
    val tokens = annotation.get(classOf[CoreAnnotations.TokensAnnotation])
    val poss = tokens.map(token => token.toString -> PartOfSpeech.withNameInsensitive(token.get(classOf[CoreAnnotations.PartOfSpeechAnnotation]))).toList
    toSymbolicSentence(Sentence(annotation.get(classOf[SentencesAnnotation]).head, sentence.toLowerCase))
  }

  def processSentence(sentence: Sentence)(implicit ec: ExecutionContext): Option[SymbolicSentence] =  {
    val annotation = new Annotation(sentence.sentence)
    SentenceActor.pipeline.annotate(annotation)
    val tokens = annotation.get(classOf[CoreAnnotations.TokensAnnotation])
    val poss = tokens.map(token => token.toString -> PartOfSpeech.withNameInsensitive(token.get(classOf[CoreAnnotations.PartOfSpeechAnnotation]))).toList
    toSymbolicSentence(sentence)
  }

  def toSymbolicSentence(sentence: Sentence): Option[SymbolicSentence] = {
    val sentenceLowerCase: Sentence = sentence.copy(sentence=sentence.sentence.toLowerCase)
    val annotation = new Annotation(sentenceLowerCase.sentence)
    SentenceActor.pipeline.annotate(annotation)
    val tokens = annotation.get(classOf[CoreAnnotations.TokensAnnotation])
    val tree = sentenceLowerCase.coremap.get(classOf[TreeCoreAnnotations.TreeAnnotation])
    val lemmas = tokens.map(token => token.toString.toLowerCase.split('-').head -> token.get(classOf[LemmaAnnotation])).toMap
    log.info("LEMMAS: " + lemmas)
    val nounPhrase = findTop(Seq("NP"), tree)
    val verbPhrase = findTop(Seq("VP"), tree)

    val subject = nounPhrase.flatMap(n =>
      findTop(SentenceActor.NounLabels, n))
    val predicate = verbPhrase.flatMap( v => findTop(SentenceActor.VerbLabels, v))

    val determiner = {
      log.info("************************NP" + nounPhrase)
      nounPhrase.flatMap(np => findPartsOfSpeech(Seq("PRPS","IN", "PRP$"), np) match {
        case Seq() => findWords(Seq("this"), np) match {
          case Seq() => subject.map { t =>
            findPartsOfSpeech(SentenceActor.NounPluralLabels, t) match {
              case Seq() => Determiner.Particular
              case _ => Determiner.General
            }
          }
          case _ => Some(Determiner.Particular)
        }
        case _ => Some(Determiner.Particular)
      })
    }

    val nounNegated = nounPhrase.map( np => findWords(SentenceActor.negation, np).nonEmpty)
    val verbNegated = verbPhrase.map(vp => findWords(SentenceActor.negation,vp).nonEmpty)

    val negated = nounNegated.flatMap(noun => verbNegated.map(verb => noun ^ verb))

    subject.flatMap(sub => predicate.map(pred => {
        val `object` = verbPhrase.map(v => findPartsOfSpeech(SentenceActor.NounLabels ++ SentenceActor.VerbLabels ++ SentenceActor.adjectiveLabels, v))
        .getOrElse(Seq())
        .flatMap(_.filter(_.isLeaf).map(_.value())).filter( _!= pred.filter(_.isLeaf).head.label.value).toSeq.map( w => lemmas.getOrElse(w, w))
      SymbolicSentence(sentence.sentence,
        determiner.getOrElse(Determiner.General),
        negated.getOrElse(false),
        {
          val subjectValue = lemmas.getOrElse(sub.filter(_.isLeaf).head.label().value(), sub.filter(_.isLeaf).head.label().value())
          findLemma(subjectValue, lemmas)
        },
        lemmas.getOrElse(pred.filter(_.isLeaf).head.label.value,pred.filter(_.isLeaf).head.label.value),
        `object`)
    }))
  }

  def findPartsOfSpeech(partsOfSpeech: Seq[String], tree: Tree): Seq[Tree] = {
    val children = tree.children().toSeq
    tree.filter( l => partsOfSpeech.contains(l.label().value())).toSeq
  }

  def findPartsOfSpeechAndWords(partsOfSpeech: Seq[String], words: Seq[String], tree: Tree): Seq[Tree] = {
    findPartsOfSpeech(partsOfSpeech, tree).filter(v => words.contains(v.value()))
  }

  def findWords(words: Seq[String], tree: Tree): Seq[Tree] = {
    val leafs = tree.filter(_.isLeaf).toSeq
    leafs.filter(v => words.contains(v.value()))
  }

  def findTop(labels: Seq[String], tree: Tree): Option[Tree] = {
    tree.find( l => labels.contains(l.label().value()))
  }

  def findBottom(labels: Seq[String], tree: Tree): Option[Tree] = {
    findTop(labels, tree).map( t => findBottomRec(labels, t))
  }

  private def findBottomRec(labels: Seq[String], tree: Tree): Tree = {
    findTop(labels, tree) match {
      case Some(t) if t!=tree => findBottomRec(labels, t)
      case _ => tree
    }
  }

  def findLemma(word: String, lemmas: Map[String, String]): String = {
    val wordLowerCase = word.toLowerCase
    lemmas.foreach( pair => {
      val sub = pair._1
      val lemma = pair._2
      try {
        sub match {
          case `wordLowerCase` =>
            log.info(s"Found lemma $lemma for word $word")
            return lemma.toString
        }
      }
      catch {
        case me: MatchError =>
          log.debug(s"Lemma for $word not found")
      }
      try {
        lemma match {
          case `wordLowerCase` =>
            return lemma.toString
        }
      }
      catch {
        case me: MatchError =>
          log.debug(s"Lemma $lemma not found")
      }
    })
    return s""
  }
}


object SentenceActor {
  val props = Props[SentenceActor]
  val annotatorProps: Properties = new Properties()
  annotatorProps.setProperty("annotators", "tokenize, ssplit, pos, lemma, parse")
  val pipeline: StanfordCoreNLP = new StanfordCoreNLP(annotatorProps)
  val IsEquivalents = Seq("exist", "happen", "remain", "stand", "represent")
  val HaveEquivalents = Seq("posess", "take", "hold", "own")
  val CanEquivalents = Seq("know", "commit", "do")
  val generalDeterminers = Seq("all", "each", "every", "any")
  val negation = Seq("no", "not", "none", "never", "n't")
  val particularDeterminers = Seq("this")
  val NounSingleLabels = Seq("NN","NNP")
  val NounPluralLabels = Seq("NNS", "NNPS")
  def NounLabels: Seq[String] = NounSingleLabels ++ NounPluralLabels
  val VerbLabels = Seq("VB","VBD","VBG","VBN","VBP","VBZ", "MD")
  val adjectiveLabels = Seq("JJ", "JJS","JJR")
}

case class Keyword(keyword: String) extends AnyVal
case class Sentence(coremap: CoreMap, sentence: String)
case class ProcessedSentence(sentence: String, tokens: List[(String, PartOfSpeech)])
case class ProcessSentenceRequest(sentence: Sentence)
case class HypothesisRequest(hypothesis: String)
case class SymbolicSentence(sentence: String, determiner: Determiner, negated: Boolean, subject: String, predicate: String, `object`: Seq[String])

sealed abstract class Determiner(override val entryName: String) extends EnumEntry

object Determiner extends Enum[Determiner] {
  case object General extends Determiner("General")
  case object Particular extends Determiner("Particular")
  override val values = findValues
}

sealed abstract class Predicate(override val entryName: String) extends EnumEntry

object Predicate extends Enum[Predicate] {
  case object Have extends Predicate("HAVE")
  case object Can extends Predicate("CAN")
  case object Be extends Predicate("BE")
  override val values = findValues
}
