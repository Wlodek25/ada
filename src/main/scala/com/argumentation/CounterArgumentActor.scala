package com.argumentation

import akka.actor.{Actor, ActorLogging, Props}
import com.ontology.OntologyManagerActor

/**
  * Created by Patryk on 2017-01-15.
  */
object CounterArgumentActor{

  val props = Props[CounterArgumentActor]
  case class CounterArgumentReceived(counterArgumentForProperty: Boolean, listOfSubjects: Seq[String], propertyValue: Boolean, isSubclass: Boolean, superClass: String, msg: SymbolicSentence, index: Integer)
  case class CounterArgumentRequest(msg: SymbolicSentence, subject: String,  index: Integer)
  case class GetObjectsWithPropertyValue(propertyName: String, propertyValue: String, msg: SymbolicSentence, index: Integer)

}

class CounterArgumentActor extends Actor with ActorLogging {

  override def receive = {
    case counterArgumentRequest : CounterArgumentActor.CounterArgumentRequest => findCounterArguments(counterArgumentRequest)
    case counterArgumentReceived : CounterArgumentActor.CounterArgumentReceived => sendCounterArgumentToDung(counterArgumentReceived)
  }

  def findCounterArguments(counterArgumentRequest: CounterArgumentActor.CounterArgumentRequest): Unit = {
    var hasPropertyCounterArgument : SymbolicSentence = null
    val index = counterArgumentRequest.index
    // utworzenie nowych aktorow, ktorzy sprawdza istnienie kontrargumentu
    val hasPropertyCounterArgumentActor = context.actorOf(OntologyManagerActor.props)
    val isSubclassCounterArgumentActor = context.actorOf(OntologyManagerActor.props)

    // wyslanie do nich wiadomosci
    val propertyName = getPropertyNameFromSymbolicSentence(counterArgumentRequest.msg)
    var propertyValue = "false"
    if(counterArgumentRequest.msg.negated) {
      propertyValue = "true"
    }

    hasPropertyCounterArgumentActor ! OntologyManagerActor.GetObjectsWithPropertyValue(propertyName, propertyValue, counterArgumentRequest.msg, counterArgumentRequest.index)

    if(counterArgumentRequest.subject != null) {
      val subClass = counterArgumentRequest.msg.subject
      val superClass = counterArgumentRequest.subject
      isSubclassCounterArgumentActor ! OntologyManagerActor.IsSubclassOf(subClass, superClass, index)
    }
  }

  def sendCounterArgumentToDung(counterArgumentReceived: CounterArgumentActor.CounterArgumentReceived): Unit = {
    val counterArgument = new StringBuilder
    var returnCounterArgument : String = null
    var symbolicSentence : SymbolicSentence = null

    if(counterArgumentReceived.msg != null) {
      if (counterArgumentReceived.counterArgumentForProperty) {
        counterArgumentReceived.listOfSubjects.foreach { sub : String =>
          symbolicSentence = new SymbolicSentence(null, counterArgumentReceived.msg.determiner, !counterArgumentReceived.msg.negated, sub, counterArgumentReceived.msg.predicate, counterArgumentReceived.msg.`object`)
          context.parent ! DungActor.CounterArgumentMessage(symbolicSentence, counterArgumentReceived.index)
        }
      }
      else {
        val obj = Seq(counterArgumentReceived.superClass)
        symbolicSentence = new SymbolicSentence(null, counterArgumentReceived.msg.determiner, true, counterArgumentReceived.msg.subject, "BE", obj)
        context.parent ! DungActor.CounterArgumentMessage(symbolicSentence, counterArgumentReceived.index)
      }
    }
    else {
      context.parent ! DungActor.CounterArgumentMessage(null, counterArgumentReceived.index)
    }
  }

  def getPropertyNameFromSymbolicSentence(sentence : SymbolicSentence) : String = {
    val stringBuilder = new StringBuilder()
    sentence.predicate.toLowerCase + sentence.`object`.lift(0).get.capitalize
  }
}
