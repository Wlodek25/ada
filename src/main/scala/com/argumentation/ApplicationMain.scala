package com.argumentation

import akka.actor.ActorSystem

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object ApplicationMain extends App {
  import com.argumentation.MainActor
  val system = ActorSystem("ArgumentationActorSystem")
  val mainActor = system.actorOf(MainActor.props, "mainActor")
  mainActor ! MainActor.Initialize
  Await.result(system.whenTerminated, Duration.Inf)
}