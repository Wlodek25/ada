package com.argumentation

import java.io.File
import java.util.Properties

import akka.actor.{Actor, ActorLogging, PoisonPill, Props, Terminated}
import com.argumentation.DocumentActor.{ProcessDocumentRequest, ProcessedMessage}
import com.argumentation.MainActor.DocumentKnowledgeExtracted
import edu.stanford.nlp.io._
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
import edu.stanford.nlp.util.CoreMap

import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.JavaConversions._
import scala.concurrent.{ExecutionContext, Future}

class DocumentActor extends Actor with ActorLogging {

  var results : Seq[String]= Seq()
  var documentName: String = _
  def name = self.path.name


  def receive = {
    case request: ProcessDocumentRequest =>
      {
        documentName = request.document.getName
        log.info("Started processing document {}", documentName)
      processDocument(request.document)
      }
    case processed: ProcessedMessage => {
      results = results :+ processed.message
      DocumentActor.countChildren += 1
      val child = context.child(processed.by)
      child.foreach { c =>
        context.watch(c)
        context.stop(c)
        log.info("received processing confirmation, stopped actor" + c.path.name)
        if (DocumentActor.countChildren == DocumentActor.numChildren) {
          context.parent ! DocumentKnowledgeExtracted(name, documentName, results.toString())
        }
      }
    }
    case Terminated(actor) => {
      log.info("received TERMINATION message from" + actor.path.name)
      if (context.children.isEmpty) {
        context.parent ! DocumentKnowledgeExtracted(name, documentName, results.toString())
      }
    }
  }

  def startProcessingSentence(sentence: CoreMap) = {
    val actor = context.actorOf(SentenceActor.props, "sentence-actor-"+sentence.toString.replaceAll(" ", ""))
    actor ! ProcessSentenceRequest(Sentence(sentence, sentence.toString))
  }

  def processDocument(file: File)(implicit ec: ExecutionContext): Future[List[CoreMap]] = Future{
    val fileRead = IOUtils.slurpFile(file)
    log.info("File read: {}", fileRead)
    processDocumentText(fileRead)
  }


  def processDocumentText(documentText: String) = {
    val annotation = new Annotation(documentText)
    DocumentActor.pipeline.annotate(annotation)
    val sentences = annotation.get(classOf[SentencesAnnotation]).toList
    log.info("sentences: {}", sentences)
    DocumentActor.numChildren = sentences.size
    sentences.foreach(startProcessingSentence)
    sentences
  }
}

object DocumentActor {
  val annotatorProps: Properties = new Properties()
  annotatorProps.put("annotators", "tokenize, ssplit, parse")
  val pipeline: StanfordCoreNLP = new StanfordCoreNLP(annotatorProps)
  val props = Props[DocumentActor]
  var numChildren = -1
  var countChildren = 0
  case class ProcessedMessage(by: String, message: String)
  case class ProcessDocumentRequest(document: File)
}

