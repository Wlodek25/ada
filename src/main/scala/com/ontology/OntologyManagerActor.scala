package com.ontology

import java.io.File

import scala.collection.JavaConverters._
import akka.actor.{Actor, ActorLogging, Kill, Props, Terminated}
import com.argumentation.CounterArgumentActor.CounterArgumentReceived
import com.argumentation.DocumentActor.ProcessedMessage
import com.argumentation._
import edu.stanford.nlp.ie.machinereading.domains.ace.reader.MatchException
import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat
import org.semanticweb.owlapi.model._
import org.semanticweb.owlapi.reasoner.InferenceType
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory
import uk.ac.manchester.cs.owl.owlapi.{OWLDataPropertyImpl, OWLObjectPropertyImpl, OWLObjectUnionOfImpl}
import org.semanticweb.owlapi.search.EntitySearcher

import scala.collection.mutable

class OntologyManagerActor extends Actor with ActorLogging {
  import OntologyManagerActor._

  var counter = 0
  val ontActor = context.actorOf(OntologyManagerActor.props, "updateOntologyActor")

  def receive = {
  	case Initialize =>
	    log.info("In UpdateOntologyActor - starting")
    case UpdateOntologyRequest(symbolicSentence) =>
      val sentence = symbolicSentence
      log.info(s"Received sentence: $sentence")
      updateKnowledgeBase(sentence)
      sender() ! DocumentActor.ProcessedMessage(context.self.path.name, symbolicSentence.toString)
      log.info(s"ONTOLOGY MANAGER ACTOR END OF KNOWLEDGE EXTRACTION: in ${self.path.name}")
    case GetObjectsWithPropertyValue(propName, propertyValue: String, sentence, argumentIndex) =>
      //val propertyName = if(propertyValue.toBoolean) { propName } else { propName + "Not"}
      val propertyName = propName
      log.info(s"Searching objects with property $propertyName equals $propertyValue")
      val listOfObjectsWithProperty = getObjectsWithPropertyValue(propertyName, propertyValue, s"", -1)
      val msg = if(listOfObjectsWithProperty.isEmpty) {
        null
      } else {
        sentence
      }
      sender ! CounterArgumentReceived(true, listOfObjectsWithProperty, propertyValue.toBoolean, false, s"", msg, argumentIndex)
    case IsSubclassOf(subClass, superClass, index) =>
      var msg: SymbolicSentence = null
      val isSubclass: Boolean = try {
        isObjectSubclassOf(subClass, superClass)
      }
      catch {
        case err: NoClassDefFoundError =>
          true
      }
      val determiner: Determiner = {
        if(!ifObjectExists(ontology, subClass)) {
          throw new NoClassDefFoundError()
        }
        if (isClass(ontology, subClass)) {
          Determiner.General
        }
        else {
          Determiner.Particular
        }
      }
      if(!isSubclass) {
        msg = SymbolicSentence(s"", determiner, isSubclass, subClass, "BE", Seq[String]() :+ superClass )
      }
      sender ! CounterArgumentReceived(false, Seq[String]() :+ subClass, false, isSubclass, superClass, msg, index)
    case Terminated(ref) => {
     log.info(s"ACTOR MANAGER TERMINATION: in ${ref.path.name}")
    }
    case _ => Unit
  }

  def updateKnowledgeBase(sentence: SymbolicSentence) = {
    log.info(s"Updating Knowledge base with sentence: $sentence")
    val subject = sentence.subject
    val predicate = sentence.predicate
    val determiner = sentence.determiner
    val negated = sentence.negated
    val `object` = sentence.`object`.lift(0).get
    val propertyName = predicate + `object`.capitalize

    log.info(s"Predicate value:${predicate.toString} , class: ${predicate.getClass.getName}")
    val ontologySubject = try {
      findSubjectInOntology(ontology, subject)
    }
    catch {
      case me: MatchException => {
        if(determiner.entryName.toLowerCase == Determiner.General.entryName.toLowerCase) {
          log.info(s"Creating class named $subject")
          createClass(ontology, subject)
        }
        if(determiner.entryName == Determiner.Particular.entryName) {
          log.info(s"Creating individual named $subject")
          createIndividual(ontology, subject)
        }
      }
    }
    val ontologyObject = try {
     findSubjectInOntology(ontology, `object`)
    }
    catch {
      case me: MatchException => {
        if(Predicate.Be.entryName == predicate.toUpperCase) {
          log.info(s"Creating class named ${`object`}")
          createClass(ontology, `object`)
        }
      }
    }

    predicate.toUpperCase match {
      case Predicate.Be.entryName => {
        log.info("UpdateOntology: Predicate BE")
        if(!negated) {
          if (isClass(ontology, subject)) {
            setSubClass(ontology, subject, `object`)
          } else {
            log.info(s"$subject is instance of class ${`object`}")
            setIndividualClass(ontology, subject, `object`)
          }
        }
      }
      case Predicate.Can.entryName | Predicate.Have.entryName => {
        log.info(s"UpdateOntology: Predicate $predicate")
        val propertyValue = !sentence.negated
        addObjectProperty(ontology, subject.capitalize, propertyName, propertyValue.toString)
      }
    }
  }

  def findSubjectInOntology(ontology: OWLOntology, subject: String): Either[OWLClass, OWLNamedIndividual] = {
    val objectName = subject.capitalize
    try {
      val ontClass = findClassInOntology(ontology, objectName)
      Left(ontClass)
    }
    catch {
      case me: MatchException => {
        log.info(s"Searching individual named $subject")
          return Right(findIndividualInOntology(ontology, objectName))
      }
    }
  }

  def findClassInOntology(ontology: OWLOntology, subject: String): OWLClass = {
    val classes = ontology.getClassesInSignature()

    classes.toArray.foreach({ontObject =>
      val ontClass = ontObject.asInstanceOf[OWLClass]
      val ontClassName = ontClass.getIRI.getShortForm.toLowerCase
      val subjectLowerCase = subject.toLowerCase
      ontClassName match {
        case`subjectLowerCase`  =>
          log.info(s"Found class $subject in knowledge base")
          return ontClass
        case notFoundSubject =>
          log.debug(s"Comparing class $notFoundSubject with $subject ")
      }
    })
    throw new MatchException(s"Class named $subject not found in knowledge base")
  }

  def findIndividualInOntology(ontology: OWLOntology, individualName: String): OWLNamedIndividual = {
    val individuals = ontology.getIndividualsInSignature()
    val individualNameLower = individualName.toLowerCase
    individuals.toArray.foreach({individual =>
      individual.asInstanceOf[OWLNamedIndividual].getIRI.getShortForm.toLowerCase match {
        case `individualNameLower` =>
          log.info(s"Found individual $individual in knowledge base")
          return individual.asInstanceOf[OWLNamedIndividual]
        case notFoundIndividual =>
          log.debug(s"Comparing individual $notFoundIndividual with $individual")
      }
    })
    throw new MatchException(s"Individual named $individualName not found in knowledge base")
  }

  def createIndividual(ontology: OWLOntology, individualName: String): Either[Unit, OWLNamedIndividual] = {
    val ontClass = findClassInOntology(ontology, "Animals")
    val newIndividual = ontologyFactory.getOWLNamedIndividual(IRI.create(s"$ns#${individualName.capitalize}"))
    val newAxiom = ontologyFactory.getOWLClassAssertionAxiom(ontClass, newIndividual)
    val ontChange = ontologyManager.addAxiom(ontology, newAxiom)
    Right(newIndividual)
  }

  def createClass(ontology: OWLOntology, className: String): Either[Unit, OWLNamedIndividual] = {
    val ontClass = findClassInOntology(ontology, "Animals")
    val newClass = ontologyFactory.getOWLClass(IRI.create(s"$ns#${className.capitalize}"))
    val newAxiom = ontologyFactory.getOWLSubClassOfAxiom(newClass, ontClass)
    val ontChange = ontologyManager.addAxiom(ontology, newAxiom)
    Left(newClass)
  }

  def setSubClass(ontology: OWLOntology, subClassName: String, superClassName: String): Unit = {
    val subClass = findClassInOntology(ontology, subClassName)
    val superClass = findClassInOntology(ontology, superClassName)
    val newAxiom = ontologyFactory.getOWLSubClassOfAxiom(subClass, superClass)
    ontologyManager.addAxiom(ontology, newAxiom)

  }

  def setIndividualClass(ontology: OWLOntology, individualName: String, individualClass: String) {
    val ontClass = findClassInOntology(ontology, individualClass)
    val individual = findIndividualInOntology(ontology, individualName)
    val newAxiom =  ontologyFactory.getOWLClassAssertionAxiom(ontClass, individual)
    ontologyManager.addAxiom(ontology, newAxiom)
  }

  def addObjectProperty(ontology: OWLOntology, objectName: String, propName: String, propertyValue: String): Unit = {
    log.info(s"addObjectProperty: add property $propName=$propertyValue to $objectName")
    val propertyLiteral = ontologyFactory.getOWLLiteral(true)
    val propertyName = {
      if(propertyValue.toBoolean) {
        propName
      }
      else {
        propName + "Not"
      }
    }
    val propertyIRI = IRI.create(s"$ontologyIRI#$propertyName")
    val ontObject = findSubjectInOntology(ontology, objectName)
    ontObject match {
      case Left(ontClass) =>
        log.info(s"Adding property $propertyName: $propertyValue to class ${ontClass.getIRI.getShortForm.toLowerCase}")
        val property = ontologyFactory.getOWLDataProperty(propertyIRI)
        val propertyHasValue = ontologyFactory.getOWLDataHasValue(property, propertyLiteral)
        val axiom = ontologyFactory.getOWLSubClassOfAxiom(ontClass, propertyHasValue)
        ontologyManager.addAxiom(ontology, axiom)
        ontologyManager.addAxiom(ontology, ontologyFactory.getOWLDataPropertyDomainAxiom(property, ontClass))
        val assertProperty = ontologyFactory.getOWLClass(objectName.capitalize)
      case Right(ontIndividual) =>
        log.info(s"Adding property $propertyName: $propertyValue to individual ${ontIndividual.getIRI.getShortForm.toLowerCase}")
        val property = ontologyFactory.getOWLDataProperty(propertyIRI)
        val axiom = ontologyFactory.getOWLDataPropertyAssertionAxiom(property, ontIndividual, propertyLiteral)
        ontologyManager.addAxiom(ontology, axiom)
        // test
        val file = new File("example_ontology.owl")
        val destination = IRI.create(file)
        ontologyManager.saveOntology(ontology, new OWLXMLDocumentFormat(), destination)
        getObjectsWithPropertyValue(propertyName, "true", "a", -1)
    }
  }

  def ifObjectExists(ontology: OWLOntology, objectName: String): Boolean = {
    try {
      findSubjectInOntology(ontology, objectName)
      log.debug(s"Object $objectName exists")
      return true
    }
    catch {
      case me: MatchException => {
        return false
      }
    }
  }

  def isObjectSubclassOf(objectName: String, className: String): Boolean = {

    val ontSuperClass = try {
      findSubjectInOntology(ontology, className)
    }
    catch {
      case me: MatchException => {
        log.info(s"Class $className not found in ontology")
        throw new NoClassDefFoundError()
      }
    }
    val objectNameLowerCase = {
      val ontSubject = try {
        findSubjectInOntology(ontology, objectName)
      }
      catch {
        case me: MatchException => {
          log.info(s"No descendant $objectName found in ontolgy")
          throw new NoClassDefFoundError()
        }
      }
      ontSubject match
      {
        case Left(ontClass) =>
          objectName.toLowerCase
        case Right(ontIndividual) =>
          var ontIndividualClassName = s""
          val ontIndividualClasses = EntitySearcher.getTypes(ontIndividual, ontology).toArray.foreach(
            ontIndividualClass => {
              ontIndividualClassName = ontIndividualClass.asInstanceOf[OWLClass].getIRI.getShortForm
              log.info(s"Individual ${objectName} class is $ontIndividualClassName")
            })
          ontIndividualClassName.toLowerCase
      }
    }

    if(objectNameLowerCase.equals(className.toLowerCase)) {
      log.info(s"${objectName} is subclass of ${className}")
      return true
    }

    ontSuperClass match {
      case Left(ontClass) =>
        val ontSubClasses = ontologyReasoner.getSubClasses(ontClass, true).getFlattened()
        ontSubClasses.toArray.foreach(f = ontSubObj => {
          val ontSubClass = ontSubObj.asInstanceOf[OWLClass]
          try {
            ontSubClass.getIRI.getShortForm.toLowerCase match {
              case `objectNameLowerCase` =>
                log.info(s"${objectName} is subclass of ${className}")
                return true
            }
          }
          catch {
            case me: MatchError =>
              log.debug(s"${objectName} is not sublcasses of ${ontSubClass.getIRI.getShortForm}")
          }
        })
        return false
      case Right(ontIndividual) =>
        val ontSubClasses = EntitySearcher.getTypes(ontIndividual, ontology)
        true
    }
  }

  def getObjectsWithPropertyValue(propName: String, propertyValue: String, sentence: String, argIndex: Int): Seq[String] = {
    var objectsWithProperties = mutable.MutableList[String]()
    val classes = ontology.getClassesInSignature()
    val propertyName = {
      if(propertyValue.toBoolean) {
        propName
      }
      else {
        propName + "Not"
      }
    }
    val propertyIRI = IRI.create(s"$ns#$propertyName")

    classes.toArray.foreach({ontObject =>
      val ontClass = ontObject.asInstanceOf[OWLClass]
      val ontClassName = ontClass.getIRI.getShortForm.toLowerCase

      ontology.getAxioms(AxiomType.OBJECT_PROPERTY_DOMAIN).toArray.foreach (
        axiom => {
          val domainAxioms = axiom.asInstanceOf[OWLObjectPropertyDomainAxiom]
          val searchedProperty = domainAxioms.getProperty.asInstanceOf[OWLObjectPropertyImpl].getIRI.getShortForm
          //log.info(s"Searching property ${searchedProperty}")
          if(searchedProperty.equals(propertyName)) {
            val classesWithProperty: List[OWLClass] = {
              if (domainAxioms.getDomain.isOWLClass) {
                val list = List[OWLClass]() :+ domainAxioms.getDomain
                list.asInstanceOf[List[OWLClass]]
              }
              else {
                var list = mutable.MutableList[OWLClass]()
                domainAxioms.getDomain.
                  asInstanceOf[OWLObjectUnionOfImpl].
                  getOperandsAsList.
                  asScala.foreach(operand => {
                  list += ontologyFactory.getOWLClass(operand.toString)
                })
                list.toList
              }
            }

            classesWithProperty.foreach(classWithProperty => {
              if (classWithProperty.equals(ontClass)) {
                domainAxioms.getDomain.getObjectPropertiesInSignature.toArray.foreach(
                  property => {
                    val objectProperty = property.asInstanceOf[OWLObjectProperty]
                    val foundPropertyName = objectProperty.getIRI.getShortForm.toLowerCase
                    if (propertyName.equals(foundPropertyName)) {
                      log.info(s"$ontClassName has property ${foundPropertyName}")
                      objectsWithProperties += ontClassName.toLowerCase
                    }
                  })
              }
            })
          }
        }
      )
      ontology.getAxioms(AxiomType.DATA_PROPERTY_DOMAIN).toArray.foreach (
        axiom => {
          val domainAxioms = axiom.asInstanceOf[OWLDataPropertyDomainAxiom]
          val searchedProperty = domainAxioms.getProperty.asInstanceOf[OWLDataPropertyImpl].getIRI.getShortForm
          if(searchedProperty.equals(propertyName)) {
            val classesWithProperty: List[OWLClass] = {
              if (domainAxioms.getDomain.isOWLClass) {
                val list = List[OWLClass]() :+ domainAxioms.getDomain
                list.asInstanceOf[List[OWLClass]]
              }
              else {
                var list = mutable.MutableList[OWLClass]()
                domainAxioms.getDomain.
                  asInstanceOf[OWLObjectUnionOfImpl].
                  getOperandsAsList.
                  asScala.foreach(operand => {
                  list += ontologyFactory.getOWLClass(operand.toString)
                })
                list.toList
              }
            }

            classesWithProperty.foreach(classWithProperty => {
              val classWithPropertyName = classWithProperty.getIRI.getShortForm.toLowerCase
              log.info(s"getObjectsWithPropertyValue: comparing class  ${ontClassName}  with $classWithPropertyName ")
              if (classWithPropertyName.equals(ontClassName.toLowerCase)) {
                log.info(s"Found class with property $propertyName: $ontClassName")
                objectsWithProperties += ontClassName.toLowerCase
              }
            })
          }
        }
      )
    })
    val instanceDataProperty = ontologyFactory.getOWLDataProperty(propertyIRI)
    val hasProperty = ontologyFactory.getOWLDataHasValue(instanceDataProperty, ontologyFactory.getOWLLiteral(true))
    //val individualsWithProperty = ontologyReasoner.getInstances(hasProperty, false).asScala

    val individualsWithProperty = classes.toArray.foreach(ontObject => {
      val ontClass = ontObject.asInstanceOf[OWLClass]
      ontologyReasoner.getInstances(ontClass, false).getFlattened.asScala.foreach(
        individual => {
          val individualName = individual.getIRI.getShortForm
          val dataProperties = ontology.getDataPropertyAssertionAxioms(individual).toArray.foreach(
            dataPropAxiom => {
              val dataProp = dataPropAxiom.asInstanceOf[OWLDataPropertyAssertionAxiom].getProperty
              val dataProperty = dataProp.asInstanceOf[OWLDataProperty]
              val individualPropertiesValues = ontologyReasoner.getDataPropertyValues(individual, dataProperty)
              individualPropertiesValues.toArray.foreach(
                individualPropertyValue => {
                  if(propertyName.toLowerCase.equals(dataProperty.getIRI.getShortForm.toLowerCase)) {
                    log.info(s"For class ${ontClass.getIRI.getShortForm} individual $individualName found ${dataProperty.getIRI.getShortForm} value ${individualPropertyValue.asInstanceOf[OWLLiteral].getLiteral}")
                    objectsWithProperties += individualName.toLowerCase
                  }
                }
              )
            }
          )

        }
      )
    })
    objectsWithProperties.distinct
  }

  def isClass(ontology: OWLOntology, className: String): Boolean = {
    try {
      findClassInOntology(ontology, className)
      true
    }
    catch {
      case me: MatchException =>
        false
    }
  }

  def isIndividual(ontology: OWLOntology, individualName: String): Boolean = {
    try {
      findIndividualInOntology(ontology, individualName)
      true
    }
    catch {
      case me: MatchException =>
        false
    }
  }
}



object OntologyManagerActor {
  val ns = "http://protege.stanford.edu/junitOntologies/testset/animals-vh.owl"
  val ontologyIRI = IRI.create(ns)
  val ontologyManager = OWLManager.createOWLOntologyManager()
  val ontologyFactory = ontologyManager.getOWLDataFactory
  val ontology = ontologyManager.loadOntology(ontologyIRI)
  val ontologyReasoner = new StructuralReasonerFactory().createReasoner(ontology)
  ontologyReasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY)

  val predicates = Predicate
  val props = Props[OntologyManagerActor]
  case object Initialize
  case class UpdateOntologyRequest(symbolicSentences: SymbolicSentence)
  case class IfObjectExists(objectName: String)
  case class GetObjectProperty(objectName: String, propertyName: String)
  case class GetObjectsWithPropertyValue(propertyName: String, propertyValue: String, sentence: SymbolicSentence, argIndex: Int)
  case class IsObjectClassOf(objectName: String, className: String)
  case class IsSubclassOf(objectName: String, className: String, argIndex: Int)
}