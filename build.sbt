name := "ada"

version := "1.0"

scalaVersion := "2.11.8"

resolvers += "Phenoscape Maven repository" at "http://phenoscape.svn.sourceforge.net/svnroot/phenoscape/trunk/maven/repository"

  val akkaV = "2.4.10"
  val stanfordNlpV = "3.6.0"
  val scalatestV = "3.0.0"
  val enumeratumV = "1.5.3"
  val scowlV = "1.1"
  val junitV = "4.12"


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaV,
  "edu.stanford.nlp" % "stanford-corenlp" % stanfordNlpV,
  "edu.stanford.nlp" % "stanford-corenlp" % stanfordNlpV classifier "models-english",
  "edu.stanford.nlp" % "stanford-parser" % stanfordNlpV,
  "com.beachape" %% "enumeratum" % enumeratumV,
  "org.phenoscape" %% "scowl" % scowlV,


  "edu.mit" % "jwi" % "2.2.3",
  "net.sourceforge.owlapi" % "jfact" % "5.0.1",
  "net.sourceforge.owlapi" % "org.semanticweb.hermit" % "1.3.8.500",
  "net.sourceforge.owlapi" % "pellet-profiler-ignazio1977" % "2.4.0-ignazio1977",
  /* Tests dependencies */
  "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
  "org.scalatest" %% "scalatest" % scalatestV % "test",
  "junit" % "junit" % junitV,
  "com.beachape" %% "enumeratum" % enumeratumV)

unmanagedJars in Compile ++= {
  val base = baseDirectory.value
  val baseDirectories = (base / "jung2")
  val customJars = (baseDirectories ** "*.jar") +++ (base / "d" / "my.jar")
  customJars.classpath
}
